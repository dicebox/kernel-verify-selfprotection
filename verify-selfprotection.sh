#!/bin/bash
# GPLv3+
# Copyright Julien Cerqueira 2019


config_file="${1}"

#rm Recommended_Settings > /dev/null 2>&1
#wget https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings > /dev/null 2>&1

while IFS= read -r line; do
  if [ "${line}" == '<h1> <span class="mw-headline" id="CONFIGs"> CONFIGs </span></h1>' ]; then
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="GCC_plugins"> GCC plugins </span></h2>' ]; then
    config_common="${section}"
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="x86_32"> x86_32 </span></h2>' ]; then
    config_gccplugins="${section}"
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="x86_64"> x86_64 </span></h2>' ]; then
    config_x86_32="${section}"
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="arm"> arm </span></h2>' ]; then
    config_x86_64="${section}"
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="arm64"> arm64 </span></h2>' ]; then
    config_arm="${section}"
    section=""
  fi
  if [ "${line}" == '<h1> <span class="mw-headline" id="kernel_command_line_options"> kernel command line options </span></h1>' ]; then
    config_arm64="${section}"
    section=""
  fi
  if [ "${line}" == '<h2> <span class="mw-headline" id="x86_64_2"> x86_64 </span></h2>' ]; then
    cmdline_common="${section}"
    section=""
  fi
  if [ "${line}" == '<h1> <span class="mw-headline" id="sysctls"> sysctls </span></h1>' ]; then
    cmdline_x86_64="${section}"
    section=""
  fi
  if [ "${line}" == "NewPP limit report" ]; then
    sysctls="${section}"
    section=""
    break
  fi
  line="$(echo "${line}" | grep -v "^<\|^#\|^$\|prior" | sed "s/ (since.*)//")"
  if [ -n "${line}" ]; then
    section="${section}\n${line}"
  fi
done < <(wget -q -O - https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings)

echo "Missing common config:"
while IFS= read -r line; do
  #echo "${line}"
  mline="$(grep "${line}" "${1}")"
  if [ -z "${mline}" ]; then
    echo "${line}"
  fi
done < <(printf "${config_common}")

echo
echo "Missing gcc plugins config:"
while IFS= read -r line; do
  #echo "${line}"
  mline="$(grep "${line}" "${1}")"
  if [ -z "${mline}" ]; then
    echo "${line}"
  fi
done < <(printf "${config_gccplugins}")

echo
echo "Missing x86_64 config:"
while IFS= read -r line; do
  #echo "${line}"
  mline="$(grep "${line}" "${1}")"
  if [ -z "${mline}" ]; then
    echo "${line}"
  fi
done < <(printf "${config_x86_64}")

#while IFS= read -r line; do
#  echo "${line}"
#done < <(printf "${cmdline_common}")

#while IFS= read -r line; do
#  echo "${line}"
#done < <(printf "${cmdline_x86_64}")

#while IFS= read -r line; do
#  echo "${line}"
#done < <(printf "${sysctls}")


#rm Recommended_Settings
